<div class="container-fluid">
      <div class="row">
        <div class="col-12">
          <h1>Student Registration</h1>
         <div class="card card-register mx-auto mt-5">
      <div class="card-header">Register a Student</div>
      <div class="card-body">
        <form action="addstudent.php" method="POST" enctype="multipart/form-data">
          <div class="form-group">
            <div class="form-row">
              <div class="col-md-6">
                <label for="name">Name</label>
                <input class="form-control" id="name" name="name" type="text" aria-describedby="nameHelp" placeholder="Enter name">
                
              </div>
              <div class="col-md-6">
                <label for="age">Age</label>
                <input class="form-control" id="age" name="age" type="text" aria-describedby="nameHelp" placeholder="Enter age">
              </div>
              </div>
              <div class="form-row">
              <div class="col-md-6">
                <label for="address">Address</label>
                <input class="form-control" id="address" name="address" type="text" aria-describedby="nameHelp" placeholder="Enter Address">
              </div>
              <div class="col-md-6">
                <label for="address">Photo</label>
                <input class="form-control" id="photo" name="photo" type="file" aria-describedby="nameHelp" placeholder="Enter Address">
              </div>

            </div>
          </div>
          <!-- <a class="btn btn-primary register" href="#">Register</a> -->
          <input type="submit" class="btn btn-primary" name="register" value="Register">
        </form>
      </div>
    </div>
    <div class="card mb-3">
        <div class="card-header">
          <i class="fa fa-table"></i> Data Table Example</div>
        <div class="card-body">
          <div class="table-responsive">
            <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
              <thead>
                <tr>
                  <th>No.</th>
                  <th>Name</th>
                  <th>Age</th>
                  <th>Address</th>
                  <th>Photo</th>
                  
                </tr>
              </thead>
              <tfoot>
                <tr>
                 <th>No.</th>
                  <th>Name</th>
                  <th>Age</th>
                  <th>Address</th>
                  <th>Photo</th>
                </tr>
              </tfoot>
              <tbody>
                   <?php
                        $myfile=fopen('student4.txt', 'r')or die('cannot open file');

                        // $i=1;
                        // while (!feof($myfile)) {
                        //   $student=fgets($myfile);
                        //   if($student){
                        //     $studentobj=json_decode($student);
                        //     $name= $studentobj->name;
                        //     $age= $studentobj->age;
                        //     $address= $studentobj->address;
                        //     $photo =$studentobj->photo;

                include('conn.php');
          $sql="select * from students"; 
          $result=$conn->query($sql);
          if($result->num_rows >0){
            $i=1;
            while($row=$result->fetch_assoc()){
              $id=$row['id'];
              $name=$row['name'];
              $age=$row['age'];
              $address=$row['address'];
              $photo=$row['photo'];        
                       

             echo "<tr><td>$i</td><td>$name</td><td>$age</td><td>$address</td><td><img src='$photo' width='100px' height='100px'></td></tr>";
                            $i++;
                          }
                        }
                   ?>


              </tbody>
            </table>
          </div>
        </div>
        <div class="card-footer small text-muted">Updated yesterday at 11:59 PM</div>
      </div>
        </div>
      </div>
    </div>
